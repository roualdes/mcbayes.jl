# Borrowed from https://github.com/Ferrite-FEM/Ferrite.jl/blob/master/docs/make.jl
using MCBayes
using Documenter
using Pkg

Pkg.precompile()

DocMeta.setdocmeta!(MCBayes, :DocTestSetup, :(using MCBayes); recursive=true)

const is_ci = true # haskey(ENV, "GITLAB_ACTIONS")

# Generate examples
include("generate.jl")

GENERATEDEXAMPLES = [joinpath("examples", f) for f in (
    "first.md",
)]

makedocs(;
    modules=[MCBayes],
    authors="Edward A. Roualdes",
    repo="https://gitlab.com/roualdes/MCBayes/blob/{commit}{path}#{line}",
    sitename="MCBayes.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://roualdes.gitlab.io/MCBayes",
        assets=String[],
    ),
    pages= Any[
        "Home" => "index.md",
        "Examples" => GENERATEDEXAMPLES
    ],
)
