```@meta
CurrentModule = MCBayes
```

# MCBayes

Documentation for [MCBayes](https://gitlab.com/roualdes/MCBayes.jl).

```@index
```

```@autodocs
Modules = [MCBayes]
```
