struct MongeStaticSampler <: AbstractMongeSampler end

function SGA(::Val{:monge_static}; kwargs...)
    f = kwargs[:f]
    data  = kwargs[:data]
    initialθ = kwargs[:initialθ]
    nt = kwargs[:nt]
    lp = Tuple(ADlp(kwargs[:AD], θ -> -f(θ, data[n]), initialθ) for n in 1:nt)
    val_grad!(lp[1], initialθ)
    r = randn(kwargs[:D])                # pca first axis
    r ./= norm(r)
    return (sampler = MongeStaticSampler(), r, ι = [1.0], kwargs..., lp)
end

function _kernel!(as::MongeStaticSampler, i, sga, θs)
    u = halton(i)
    j = sga.jitter == :uniform ? u : -log(u)
    L = max(1, ceil(Int, j * sga.knobs.T / sga.knobs.ε))
    nt = sga.nt
    @sync for it in 1:nt
        Threads.@spawn for c in it:nt:sga.chains
            info = lmc_static!(i, θs[c][i-1], θs[c][i], sga.rng[it], sga.D, sga.M,
                        sga.knobs.ε, sga.ι[1], L, sga.maxdeltaH, sga.lp[it], sga.data[it])
            updateinfo!(i, c, sga, (info..., maxtrajectorylength = sga.knobs.T))
        end
    end
    sga.adaptchains && adaptchains!(i, sga, θs)
end

function lmc_static!(i, θ1, θ2, rng, D, M, ε, ι, L, maxdeltaH, lp, data)
    T = eltype(θ1)
    q = copy(θ1)

    ι2 = ι ^ 2
    val_grad!(lp, q)
    glp = grad(lp)
    Nglp = norm(glp) ^ 2
    Lι = 1 + ι2 * Nglp
    m = sqrt(Lι)
    glp .= grad(lp) .* m
    H = M .* m

    v = generatevelocity(rng, glp, Nglp, ι, D)
    E1 = energy(val(lp), glp, Nglp, ι, v)
    isnan(E1) && (E1 = typemax(E1))

    logdet = velocityhalfstep!(v, glp, H, Lι, ι, 0.5 * ε, data)

    for l in 1:L
        @. q += ε * v
        val_grad!(lp, q)
        glp .= grad(lp)
        Nglp = norm(glp) ^ 2
        Lι = 1 + ι2 * Nglp
        m = sqrt(Lι)
        glp .= grad(lp) .* m
        H .= M .* m
        if l != L
            logdet += velocityhalfstep!(v, glp, H, Lι, ι, ε, data)
        end
    end

    logdet += velocityhalfstep!(v, glp, H, Lι, ι, 0.5 * ε, data)

    E2 = energy(val(lp), glp, Nglp, ι, v)
    isnan(E2) && (E2 = typemax(T))
    divergent = divergence(E2, E1, maxdeltaH)

    Δ = E1 - E2 + logdet
    u = isfinite(Δ) ? min(one(Δ), exp(Δ)) : zero(Δ)
    accepted = rand(rng, T) < u
    @. θ2 = q * accepted + θ1 * (1 - accepted)

    return (accepted = accepted,
            stepsize = ε,
            acceptstat = u,
            q = q,
            p = v,
            leapfrog = L,
            divergence = divergent)
end

# distance between manifold-norms
# (||z'||_G^2 - ||z||_G^2)^2
# function _sampler_gradient!(as::MongeStaticSampler, i, sga, θs, mθ, mq)
#     t = sga.knobs.T + sga.knobs.ε # [4]#L643
#     h = halton(i)
#     ι = sga.ι[1]
#     lp = sga.lp[1]
#     q = similar(θs[1][1])
#     gq = similar(q)
#     θ = similar(q)
#     gθ = similar(q)
#     Gq = similar(q)
#     @inbounds @simd for c in 1:sga.chains
#         q .= view(sga.q, :, c) .- mq
#         θ .= θs[c][i-1] .- mθ
#         val_grad!(lp, θ)
#         Gθ = _G(θ, ι, grad(lp))
#         val_grad!(lp, q)
#         Gq .= _G(q, ι, grad(lp))
#         dsq = q' * Gq - θ' * Gθ
#         sga.ghats[c] = 4dsq * Gq' * _G(view(sga.p, :, c), ι, grad(lp)) - dsq ^ 2 / t
#         sga.ghats[c] *= h
#     end
# end
