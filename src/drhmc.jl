# Adapted from
# [1] https://github.com/modichirag/hmc/blob/main/src/pyhmc.py
# [2] https://arxiv.org/abs/2110.00610

abstract type AbstractDelayedRejectionMEADSSampler <: AbstractMEADSSampler end
struct DelayedRejectionMEADSSampler <: AbstractDelayedRejectionMEADSSampler end

function DRHMC(initialθ, f, data = nothing;
               D = length(initialθ),
               S = eltype(initialθ),
               iterations = 1000,
               warmup = iterations,
               folds = 4,
               chainsperfold = 32,
               chains = folds * chainsperfold,
               adam_steps = 100,
               a = 5,           # TODO(ear) what of one step-reducer per chain? fold?
               J = 3,           # TODO(ear) what of one num_steps per chain? fold?
               AD = :forward,
               σ = ones(S, D, folds),
               ε = 0.5 .* ones(S, folds),
               εf = 0.5,
               γ = 1 ./ ε,
               α = 1 .- exp.(-2 .* γ .* ε),
               δ = 0.5 .* α,
               L = 1,
               η = 0.05,
               β1 = 0.9,
               β2 = 0.999,
               τ = 1e-08,
               nru = true,
               adaptσ = true,
               adaptε = true,
               adaptα = true,
               adaptγ = true,
               adaptδ = true,
               nt = Threads.nthreads(),
               maxdeltaH = 1000,
               kwargs...)

    datas = Tuple(deepcopy(data) for _ in 1:nt)
    rng = Tuple(Random.Xoshiro(rand(UInt32)) for _ in 1:chains)

    adstan = AD == :stan
    lp = if adstan
        lib = Libc.Libdl.dlopen(f)
        Tuple(ADlp(AD, lib, data) for n in 1:nt)
    else
        Tuple(ADlp(AD, θ -> f(θ, datas[n]), initialθ) for n in 1:nt)
    end

    trajectorylengths = []
    grads = ones(S, D, chains)
    m = zeros(S, D, chains)
    u = zeros(S, chains)
    for c in 1:chains
        m[:, c] = randn(rng[c], S, D)
        u[c] = 2 * rand(rng[c], S) - 1
    end

    partition = _shuffle_folds(Random.Xoshiro(rand(UInt32)), chains, chainsperfold)

    I = warmup + iterations
    allem = Array{S}(undef, I, chains, D)
    accepted = zeros(Bool, chains, I)
    acceptanceprob = zeros(S, chains, I)
    divergence = zeros(Bool, chains, I)
    return (sampler = DelayedRejectionMEADSSampler(),
            initialθ,
            f,
            AD,
            D,
            iterations,
            warmup,
            folds,
            chainsperfold,
            chains,
            partition,
            adam_steps,
            grads,
            nt,
            datas,
            rng,
            u,
            m,
            δ,
            α,
            lp,
            σ,
            ε,
            εf,
            γ,
            L,
            a,
            J,
            η,
            β1,
            β2,
            τ,
            adaptσ,
            adaptε,
            adaptα,
            adaptγ,
            adaptδ,
            nru,
            allem,
            accepted,
            acceptanceprob,
            divergence,
            maxdeltaH,
            trajectorylengths,
            kwargs...)
end

function _kernel!(as::DelayedRejectionMEADSSampler, i, drhmc, θs)

    if i % drhmc.folds == 0
        srng = Random.Xoshiro(rand(UInt32))
        drhmc.partition .= _shuffle_folds(srng, drhmc.chains, drhmc.chainsperfold)
    end

    skipfold = i % drhmc.folds + 1
    nt = drhmc.nt
    @sync for it in 1:nt
        Threads.@spawn for f in it:nt:length(drhmc.partition)

            k = (f + 1) % drhmc.folds + 1
            kfold = drhmc.partition[k]

            Θ, σ = _standardize_draws(θs, kfold)
            drhmc.adaptσ && _update_metric!(drhmc.sampler, i, drhmc, f, σ)
            drhmc.adaptε && _update_stepsize!(drhmc.sampler, i, drhmc, f, θs, kfold, σ, it)
            drhmc.adaptγ && _update_damping!(i, drhmc, f, Θ)
            drhmc.adaptα && _update_noise!(i, drhmc, f)
            drhmc.adaptδ && _update_drift!(i, drhmc, f)

            fold = drhmc.partition[f]
            for c in fold
                if f != skipfold
                    @views info = drhmc!(i, θs[c][1], θs[c][2], drhmc.rng[c], drhmc.D,
                                         drhmc.σ[:, f], drhmc.ε[f], drhmc.L, drhmc.a,
                                         drhmc.J, drhmc.u[c:c], drhmc.δ[f], drhmc.nru,
                                         drhmc.m[:, c], drhmc.α[f],
                                         drhmc.maxdeltaH, drhmc.lp[it], drhmc.datas[it])
                    updateinfo!(i, c, drhmc, info)
                else
                    θs[c][2] .= θs[c][1]
                end
            end
        end
    end
end

function drhmc!(i, θ1, θ2, rng, D, σ, ε, L, a, J, u, δ, nru, m, α, maxdeltaH, lp, data)
    T = eltype(θ1)
    avec = zeros(T, J)
    ufac = zero(T)
    ptries = ones(T, J)
    mt = copy(m)
    allem = copy(θ1)

    val_grad!(lp, θ1)
    H0 = _hamiltonian(val(lp), m)

    e = ε .* σ
    q1, m1 = _leapfrog(θ1, m, e, L, lp)
    H1 = _hamiltonian(val(lp), m1)

    allem .= q1
    divergent = divergence(H1, H0, maxdeltaH)

    pfac = exp(H0 - H1)
    prob = pfac
    if isnan(prob) || isinf(prob) || isapprox(θ1, q1)
        prob = zero(T)
    end

    ufac = prob
    prob = min(1, prob)
    avec[1] = prob

    if isnan(pfac) || isinf(pfac)
        ptries[1] = one(T)
    else
        ptries[1] = 1 - avec[1]
    end

    # accepted = rand(rng, T) < avec[1]
    accepted = abs(u[1]) < avec[1]

    # TODO(ear) push this into a vector of unknown size
    # so can make a pretty plot of clumped rejections when the
    # probability of acceptance is high:
    # Y acceptance probs by X iterations; mark red the rejections
    v = zero(T)

    if accepted
        θ2 .= q1
        m .= m1
    else
        for j in 2:J
            if rand(rng, T) > ptries[j - 1]
                θ2 .= θ1
                m .= -mt
                break
            end

            aj = a ^ (j - 1)

            val_grad!(lp, θ1)
            # qj, mj = _leapfrog(θ1, m, e ./ aj, L * aj, lp)
            qj, mj = _leapfrog(θ1, m, e ./ aj, L, lp)

            Hj = _hamiltonian(val(lp), mj)
            divergent = divergence(Hj, H0, maxdeltaH)

            pfac = exp(H0 - Hj)
            if isapprox(θ1, qj)
                pfac = zero(T)
            end

            jdx = 1:(j - 1)
            den = prod(1 .- avec[jdx]) * prod(ptries[jdx])
            num = _get_num(j, qj, -mj, L, e, a, lp, maxdeltaH)

            prob = pfac * num / den
            if isnan(prob) || isinf(prob)
                prob = zero(T)
            end
            ufac = prob
            avec[j] = min(1, prob)

            if isinf(pfac) || isnan(pfac)
                ptries[j] = one(T)
            else
                ptries[j] = 1 - avec[j]
            end

            u[1] = nru ? (u[1] + 1 + δ) % 2 - 1 : rand(rng, T)
            accepted = abs(u[1]) < avec[j]
            allem .= qj

            if accepted
                θ2 .= qj
                m .= mj
                break
            end
        end
        if !accepted
            θ2 .= θ1
            m .= -mt
        end
    end

    @. u = u / ufac * accepted + u * (1 - accepted)
    v = abs(u[1])
    u[1] = nru ? (u[1] + 1 + δ) % 2 - 1 : rand(rng, T)
    m .= sqrt(1 - α ^ 2) .* m .+ α .* randn(rng, T, D)
    return (accepted = accepted, divergence = divergent, v = v, allem = allem)
end

function _get_num(J_, q_, m_, L_, e_, a_, lp_, maxdeltaH_)
    T = eltype(q_)
    avec = zeros(T, J_)
    ptries = ones(T, J_)

    val_grad!(lp_, q_)
    H0 = _hamiltonian(val(lp_), m_)

    for j in 1:J_
        aj = a_ ^ (j - 1)
        val_grad!(lp_, q_)
        # qj, mj = _leapfrog(q_, m_, e_ ./ aj, L_ * aj, lp_)
        qj, mj = _leapfrog(q_, m_, e_ ./ aj, L_, lp_)

        Hj = _hamiltonian(val(lp_), mj)
        divergent = divergence(Hj, H0, maxdeltaH_)
        pfac = exp(H0 - Hj)

        if isapprox(q_, qj)
            pfac = zero(T)
        end

        prob = zero(T)
        if j > 1
            jdx = 1:(j - 1)
            den = prod(1 .- avec[jdx]) * prod(ptries[jdx])
            num = _get_num(j - 1, qj, -mj, L_, e_, a_, lp_, maxdeltaH_)
            prob = pfac * num / den
        else
            prob = pfac
        end

        if isinf(prob) || isnan(prob)
            return zero(T)
        else
            avec[j] = min(1, prob)
        end

        if isinf(pfac) || isnan(pfac)
            ptries[j] = one(T)
        else
            ptries[j] = 1 - avec[j]
        end

        pa = prod(1 .- avec)
        if pa > zero(T)
            return pa
        end
    end

    return prod(1 .- avec) * prod(ptries)
end

function _hamiltonian(_lp, _m)
    T = eltype(_lp)
    onehalf = convert(T, 0.5)
    H = _lp + onehalf * (_m' * _m)
    isnan(H) && (H = typemax(T))
    return H
end

function _leapfrog(_q, _p, _ε, _L, _lp)
    T = eltype(_q)
    onehalf = convert(T, 0.5)
    q = copy(_q)
    g = grad(_lp)

    p = _p .- onehalf .* _ε .* g

    @inbounds for l in 1:_L
        @. q += _ε .* p
        val_grad!(_lp, q)
        g = grad(_lp)
        if l != _L
            @. p -= _ε * g
        end
    end

    @. p -= onehalf * _ε * g

    return q, p
end
