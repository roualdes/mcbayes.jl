# Adapted from https://arxiv.org/pdf/2102.04509.pdf
struct GWGBinarySampler <: AbstractSampler end

function GWGBinary(initialθ,
                   parameters;
                   iterations = 2000,
                   warmup = div(iterations, 2),
                   idx = getindices(initialθ, parameters),
                   pdim = length(idx),
                   rng = Random.MersenneTwister(rand(1:typemax(Int)))) # TODO snake rng through methods
    return (sampler = GWGBinarySampler(),
            parameters = parameters,
            iterations = iterations,
            warmup = warmup,
            accepted = zeros(Bool, iterations))
end

function _kernel!(as::GWGBinarySampler, i, gwg, θs, lpgrad;
                  data = nothing, kwargs...)
    old = θs[i-1]
    new = θs[i]
    idx = gwg.idx

    # Adapted from
    # https://github.com/wgrathwohl/GWG_release/blob/main/samplers.py
    forward_delta = diff_fn(gwg, old, lpgrad, data = data)
    changes = rand_onehotlogits(forward_delta)
    i = findfirst(==(1), changes)
    lp_forward = logsoftmax(forward_delta)[i] # normalized Categorical distribution

    new[idx] .= (1 .- old[idx]) .* changes .+ old[idx] .* (1 .- changes) # xor(old, changes), flipbit

    reverse_delta = diff_fn(gwg, new, lpgrad, data = data)
    lp_reverse = logsoftmax(reverse_delta)[i]

    a = first(lp(new, data)) - first(lp(old, data)) + lp_reverse - lp_forward
    accepted = log(rand()) < a
    new[idx] .= new[idx] * accepted + old[idx] * (1 - accepted)

    return (accepted = accepted, )
end

function diff_fn(gwg, θ, lpgrad; data = nothing)
    glp = last(lpgrad(θ, data))
    return @. -(2θ[gwg.idx] - 1) * glp[gwg.idx]
    # gp[gwg.idx] enables one gradient for all parameters.  Good and bad.
    # Good:  simpler.
    # Bad: order, with respect to initialdraw, matters (impacts HMC, too).
    # Costly, since all gradients are computed when only gwg.idx's gradients are needed.
end

function _updateinfo!(as::GWGBinarySampler, i, gwg, info)
    gwg.accepted[i] = info.accepted
end
