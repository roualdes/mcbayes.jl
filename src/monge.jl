abstract type AbstractMongeSampler <: AbstractSNAPERSampler end
struct MongeSampler <: AbstractMongeSampler end

function SGA(::Val{:monge}; kwargs...)
    f = kwargs[:f]
    data  = kwargs[:data]
    initialθ = kwargs[:initialθ]
    nt = kwargs[:nt]
    lp = Tuple(ADlp(:forward_h, θ -> -f(θ, data[n]), initialθ) for n in 1:nt)
    val_hess!(lp[1], initialθ)
    r = randn(kwargs[:D])                # pca first axis
    r ./= norm(r)
    return (sampler = MongeSampler(), r, ι = [1.0], kwargs..., lp)
end

function _kernel!(as::MongeSampler, i, sga, θs)
    u = halton(i)
    j = sga.jitter == :uniform ? u : -log(u)
    L = max(1, ceil(Int, j * sga.knobs.T / sga.knobs.ε))
    nt = sga.nt
    @sync for it in 1:nt
        Threads.@spawn for c in it:nt:sga.chains
            info = lmc!(i, θs[c][i-1], θs[c][i], sga.rng[it], sga.D, sga.M,
                        sga.knobs.ε, sga.ι[1], L, sga.maxdeltaH, sga.lp[it], sga.data[it])
            updateinfo!(i, c, sga, (info..., maxtrajectorylength = sga.knobs.T))
        end
    end
    sga.adaptchains && adaptchains!(i, sga, θs)
end

# function _adaptchains!(as::AbstractMongeSampler, i, sga, θs)
#     if i <= sga.warmup
#         αs = @view sga.acceptstat[:, i]
#         αbar = harmonicmean(αs)

#         if i > 100 && sga.adaptT
#             _update_trajectorylength!(i, sga, θs, αs, αbar)
#         end

#         sga.adaptε && _update_stepsize!(i, sga, αbar)
#         sga.adaptM && _update_metric!(i, sga, θs)
#         sga.adaptr && _update_pca!(sga.sampler, i, sga, θs)

#         i == sga.resetM && reset!(sga.rm)
#         i == sga.resetT && (sga.knobs.T = 0)
#         i == sga.resetε && (sga.knobs.ε = 0)

#         if i == sga.warmup
#             sga.knobs.ε = sga.knobs.εbar
#             sga.knobs.T = sga.knobs.Tbar
#         end
#     end
# end

function lmc!(i, θ1, θ2, rng, D, M, ε, ι, L, maxdeltaH, lp, data)
    T = eltype(θ1)
    q = copy(θ1)

    ι2 = ι ^ 2
    val_hess!(lp, q)
    glp = grad(lp)
    Nglp = norm(glp) ^ 2
    Lι = 1 + ι2 * Nglp
    m = sqrt(Lι)
    glp .= grad(lp) ./ m
    H = hess(lp) ./ m

    v = generatevelocity(rng, glp, Nglp, ι, D)
    E1 = energy(val(lp), glp, Nglp, ι, v)
    isnan(E1) && (E1 = typemax(E1))

    logdet = velocityhalfstep!(v, glp, H, Lι, ι, 0.5 * ε, data)

    for l in 1:L
        @. q += ε * v
        val_hess!(lp, q)
        glp .= grad(lp)
        Nglp = norm(glp) ^ 2
        Lι = 1 + ι2 * Nglp
        m = sqrt(Lι)
        glp .= grad(lp) ./ m
        H .= hess(lp) ./ m
        if l != L
            logdet += velocityhalfstep!(v, glp, H, Lι, ι, ε, data)
        end
    end

    logdet += velocityhalfstep!(v, glp, H, Lι, ι, 0.5 * ε, data)

    E2 = energy(val(lp), glp, Nglp, ι, v)
    isnan(E2) && (E2 = typemax(T))
    divergent = divergence(E2, E1, maxdeltaH)

    Δ = E1 - E2 + logdet
    u = isfinite(Δ) ? min(one(Δ), exp(Δ)) : zero(Δ)
    accepted = rand(rng, T) < u
    @. θ2 = q * accepted + θ1 * (1 - accepted)

    return (accepted = accepted,
            stepsize = ε,
            acceptstat = u,
            q = q,
            p = v,
            leapfrog = L,
            divergence = divergent)
end

_G(x, ι, glp) = x .+ ι^2 .* glp .* (glp' * x)

# distance between manifold-norms
# (||z'||_G^2 - ||z||_G^2)^2
# function _sampler_gradient!(as::MongeSampler, i, sga, θs, mθ, mq)
#     t = sga.knobs.T + sga.knobs.ε # [4]#L643
#     h = halton(i)
#     ι = sga.ι[1]
#     lp = sga.lp[1]
#     q = similar(θs[1][1])
#     gq = similar(q)
#     θ = similar(q)
#     gθ = similar(q)
#     Gq = similar(q)
#     @inbounds @simd for c in 1:sga.chains
#         q .= view(sga.q, :, c) .- mq
#         θ .= θs[c][i-1] .- mθ
#         val_hess!(lp, θ)
#         Gθ = _G(θ, ι, grad(lp))
#         val_hess!(lp, q)
#         Gq .= _G(q, ι, grad(lp))
#         dsq = q' * Gq - θ' * Gθ
#         sga.ghats[c] = 4dsq * Gq' * _G(view(sga.p, :, c), ι, grad(lp)) - dsq ^ 2 / t
#         sga.ghats[c] *= h
#     end
# end

function generatevelocity(rng, glp, Nl, ι, D)
    c = zero(eltype(glp))
    z = randn(rng, D)
    if Nl < 1e-8
        c = -0.5 * ι ^ 2
    else
        Lι = 1 + ι ^ 2 * Nl
        c = 1 / sqrt(Lι) - 1
        c *= Lι / Nl
    end
    return z .+ c .* glp .* (glp' * z)
end

function energy(lp, glp, Nl, ι, v)
    T = eltype(lp)
    Nv = zero(T)
    dlv = zero(T)
    @inbounds @simd for i in eachindex(glp, v)
        Nv += v[i] ^ 2
        dlv += glp[i] * v[i]
    end
    ι2 = ι ^ 2
    nl = ι2 * Nl
    return -lp + 0.5 * (Nv + ι2 * (1 + nl) * dlv ^ 2 - log1p(nl))
end

function velocityhalfstep!(v, glp, H, Lι, ι, ε, data)
    ι2 = ι ^ 2
    sqrtLι = sqrt(Lι)

    vH = ε * v' * H
    lH = ε * ι2 * glp' * H
    lv = glp' * v

    logdet = -log(abs(1 + lH * v))

    h = -reshape(lH, :) + (ε * sqrtLι + ι2 * Lι * lv) .* glp
    n = (glp .+ reshape(vH, :))' * h
    d = 1 / (glp' * glp + vH * glp + 1 / (ι2 * Lι))
    h .-= glp * n * d

    d *= lv + vH * v
    @. v += h - d * glp

    logdet += log(abs(1 - lH * v))
    return logdet
end
