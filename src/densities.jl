function normal(y, l, s)
    y, l, s = promote(y, l, s)
    onehalf = oftype(y, 0.5)
    log2pi = log(2 * oftype(y, pi))
    d = (y - l) / s
    return -log(s) - onehalf * (log2pi + d * d)
end

function grad_normal(y, l, s)
    invs = 1 / s
    d = (y - l) * invs
    dy = -d * invs
    dl = -dy
    ds = -invs + d * d * invs
    return dy, dl, ds
end

function mvnormal(y, l, s)
    # TODO add logabsdet(s), from LinearAlgebra
    d = y - l
    return -0.5 * length(y) * log(2 * pi) + -0.5 *  d' * (s \ d)
end

function mvnormal(z, y, l, s::AbstractMatrix)
    # TODO add logabsdet(s), from LinearAlgebra
    d = y - l
    ldiv!(z, cholesky(s), d)
    return -0.5 * length(y) * log(2 * pi) + -0.5 * d' * z
end

function mvnormal(z, y, l, s::Union{Cholesky, Diagonal})
    # TODO add logabsdet(s), from LinearAlgebra
    d = y - l
    ldiv!(z, s, d)
    return -0.5 * length(y) * log(2 * pi) + -0.5 * d' * z
end

function grad_mvnormal(y, l, s)
    d = y - l
    dy = -s \ d
    dl = -dy
    ds = 0.0 # TODO wrong!
    return dy, dl, ds
end

function grad_mvnormal(z, y, l, s::Union{Cholesky, Diagonal})
    d = y - l
    ldiv!(z, s, d)
    dl = z
    ds = 0.0 # TODO wrong!
    return -z, dl, ds
end

function grad_mvnormal(z, y, l, s::AbstractMatrix)
    d = y - l
    ldiv!(z, cholesky(s), d)
    dl = z
    ds = 0.0 # TODO wrong!
    return -z, dl, ds
end

function H_mvnormal(y, l, s)
    return -Diagonal(ones(length(y))), -Inf, -Inf
end

function H_mvnormal(z, y, l, s)
    z[diagind(z)] .= -1
    return z, -Inf, -Inf
end

function halfnormal(y, s)
    t = normal(y, 0, s)
    return oftype(t, log(2)) + t
end

function grad_halfnormal(y, s)
    gn = grad_normal(y, 0, s)
    return first(gn), last(gn)
end


function gamma(y, a, b)
    return a * log(b) - loggamma(a) + (a - 1) * log(y) - b * y
end

function grad_gamma(y, a, b)
    dy = (a - 1) / y - b
    da = log(b) - digamma(a) + log(y)
    db = a / b - y
    return dy, da, db
end

function invgamma(y, a, b)
    return a * log(b) - loggamma(a) - (a + 1) * log(y) - b / y
end

function grad_invgamma(y, a, b)
    invy = 1 / y
    dy = -(a + 1) * invy + b * invy * invy
    da = log(b) - digamma(a) - log(y)
    db = a / b - 1 * invy
    return dy, da, db
end

function poisson(y, l)
    return y * log(l) - loggamma(y+1) - l
end

function grad_poisson(y, l)
    dy = log(l) - digamma(y + 1)
    dl = y / l - 1
    return dy, dl
end

function binomial(y, k, p)
    b = loggamma(k+1) - loggamma(y + 1) - loggamma(k - y + 1)
    return b + y * log(p) + (k - y) * log(1 - p)
end

function grad_binomial(y, k, p)
    dy = digamma(k - y + 1) - digamma(y + 1) + log(p / (1 - p))
    dk = digamma(k + 1) - digamma(k - y + 1) + log(1 - p)
    dp = (y - p * k) / (p * (1 - p))
    return dy, dk, dp
end

function beta(y, a, b)
    return -logbeta(a, b) + (a - 1) * log(y) + (b - 1) * log(1 - y)
end

function grad_beta(y, a, b)
    dy = (a - a*y - 1 + 2y - b*y) / (y * (1 - y))
    digab = digamma(a + b)
    da = digab - digamma(a) + log(y)
    db = digab - digamma(b) + log(1 - y)
    return dy, da, db
end

function bernoulli(y, p)
    return y * log(p) + (1 - y) * log(1 - p)
end

function grad_bernoulli(y, p)
    gy = log(p / (1 - p))
    gp = y / p + (y - 1) / (1 - p)
    return gy, gp
end

# TODO add bernoulli_logit_glm from https://github.com/stan-dev/math/blob/develop/stan/math/prim/prob/bernoulli_logit_glm_lpmf.hpp
function bernoulli_logit(y, a)
    # adapted from
    # https://lingpipe-blog.com/2012/02/16/howprevent-overflow-underflow-logistic-regression/
    # https://github.com/stan-dev/math/blob/develop/stan/math/prim/prob/bernoulli_logit_lpmf.hpp
    sa = (2y - 1) * a
    cutoff = oftype(a, 20)
    if sa > cutoff
        return exp(-sa)
    elseif sa < -cutoff
        return sa
    else
        return -log1p(abs(exp(-sa)))
    end
end

function grad_bernoulli_logit(y, a)
    # adapted from
    # https://github.com/stan-dev/math/blob/develop/stan/math/prim/prob/bernoulli_logit_lpmf.hpp
    s = 2y - 1
    cutoff = oftype(a, 20)
    sa = s * a
    da = zero(a)
    if sa > cutoff
        da = -exp(-sa)
    elseif sa < -cutoff
        da = s
    else
        exp_nsa = exp(-sa)
        da = s * exp_nsa / (1 + exp_nsa)
    end
    dy = a
    return dy, da
end

function studentt(y, l, s, v)
    y, l, s, v = promote(y, l, s, v)
    onehalf = oftype(y, 0.5)
    vd2 = onehalf * v
    vp1d2 = vd2 + onehalf
    c = loggamma(vp1d2) - loggamma(vd2) - onehalf * log(v * pi) - log(s)
    z = (y - l) / s
    return c - vp1d2 * log1p(z * z / v)
end

function grad_studentt(y, l, s, v)
    y, l, s, v = promote(y, l, s, v)
    onehalf = oftype(y, 0.5)
    inv_v = 1 / v
    inv_s = 1 / s
    vd2 = onehalf * v
    vp1d2 = vd2 + onehalf
    z = (y - l) * inv_s
    z2 = z * z
    dy = -vp1d2 * 2z / (v + z2)
    dl = -dy
    ds = (2dl - 1) * inv_s
    dcdv = onehalf * (digamma(vp1d2) - digamma(vd2) -  inv_v)
    dv = dcdv - onehalf * log1p(z2 * inv_v) + vp1d2 * z2 / (v * v + v * z2)
    return dy, dl, ds, dv
end

# TODO complete these
function halfstudentt(y, s, v)
    t = studentt(y, 0, s, v)
    return convert(typeof(t), log(2)) + t
end

function grad_halfstudentt(y, s, v)
    gt = grad_studentt(y, 0, s, v)
    sv = last(gt, 2)
    return first(gt), first(sv), last(sv)
end
