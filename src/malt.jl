struct MALTSampler <: AbstractSNAPERSampler end

function SGA(::Val{:malt}; kwargs...)
    r = randn(kwargs[:D])                # pca first axis
    r ./= norm(r)
    return (sampler = MALTSampler(), r, kwargs...)
end

function _kernel!(as::MALTSampler, i, sga, θs)
    u = halton(i)
    j = sga.jitter == :uniform ? 2u : -log(u)
    L = max(1, ceil(Int, j * sga.knobs.T / sga.knobs.ε))
    nt = sga.nt
    @sync for it in 1:nt
        Threads.@spawn for c in it:nt:sga.chains
            info = phmc!(i, θs[c][i-1], θs[c][i], sga.rng[it], sga.D, sga.M,
                        sga.knobs.ε, L, sga.maxdeltaH, sga.lp[it], sga.data[it])
            updateinfo!(i, c, sga, (info..., maxtrajectorylength = sga.knobs.T))
        end
    end
    sga.adaptchains && adaptchains!(i, sga, θs)
end

function phmc!(i, θ1, θ2, rng, D, M, ε, L, maxdeltaH, lp, data)
    T = eltype(θ1)
    q = copy(θ1)

    φ = 0 # 2 #/ sqrt(maximum(M))
    # TODO 0 is the only thing that works reasonable well
    # my implementation is probably wrong somewhere
    η = exp(-φ * 0.5 * ε)
    ρ = sqrt(1 - η ^ 2)
    p = randn(rng, D)

    val_grad!(lp, q)
    glp = grad(lp)
    H1 = hamiltonian(val(lp), p)
    isnan(H1) && (H1 = typemax(T))

    m = sqrt.(M)
    e = ε .* m ./ maximum(m)

    for l in 1:L
        p .= η .* p .+ ρ .* randn(rng, D)

        p .-= 0.5 * e .* glp
        @. q += e * p

        val_grad!(lp, q)
        glp .= grad(lp)

        p .-= 0.5 * e .* glp
        p .= η .* p .+ ρ .* randn(rng, D)
    end

    H2 = hamiltonian(val(lp), p)
    isnan(H2) && (H2 = typemax(T))
    divergent = divergence(H2, H1, maxdeltaH)

    α = min(1, exp(H1 - H2))
    accepted = rand(rng, T) < α
    @. θ2 = q * accepted + θ1 * (1 - accepted)

    return (accepted = accepted,
            stepsize = ε,
            acceptstat = α,
            q = q,
            p = p,
            leapfrog = L,
            divergence = divergent)
end
