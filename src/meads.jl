# Adapted from
# [1] https://proceedings.mlr.press/v151/hoffman22a/hoffman22a.pdf
# [2] https://github.com/tensorflow/probability/blob/main/discussion/meads/meads.ipynb

abstract type AbstractMEADSSampler <: AbstractSampler end
struct MEADSSampler <: AbstractMEADSSampler end

function MEADS(initialθ, f, data = nothing;
               D = length(initialθ),
               S = eltype(initialθ),
               iterations = 1000,
               warmup = iterations,
               # TODO should this be however many folds you really want + 1
               folds = 4,
               # since 1 fold is skipped each iteration?
               # otherwise not utilizing the CPU cores effectively, ya?
               chainsperfold = 32,
               chains = folds * chainsperfold,
               σ = ones(S, D, folds),
               AD = :forward,
               ε = 0.5 .* ones(S, folds),
               εf = 0.5,
               γ = 1 ./ ε,
               α = 1 .- exp.(-2 .* γ .* ε),
               δ = 0.5 .* α,
               adam_steps = 100,
               η = 0.05,
               β1 = 0.9,
               β2 = 0.999,
               τ = 1e-08,
               adaptε = true,
               adaptσ = true,
               adaptγ = true,
               adaptα = true,
               adaptδ = true,
               adaptfolds = true,
               nru = true,
               nt = Threads.nthreads(),
               maxdeltaH = 1000,
               kwargs...)

    datas = Tuple(deepcopy(data) for _ in 1:nt)
    rng = Tuple(Random.Xoshiro(rand(UInt32)) for _ in 1:chains)

    adstan = AD == :stan
    lp = if adstan
        lib = Libc.Libdl.dlopen(f)
        Tuple(ADlp(AD, lib, data) for n in 1:nt)
    else
        Tuple(ADlp(AD, θ -> f(θ, datas[n]), initialθ) for n in 1:nt)
    end

    trajectorylengths = []
    grads = ones(S, D, chains)
    m = zeros(S, D, chains)
    u = zeros(S, chains)
    for c in 1:chains
        m[:, c] = randn(rng[c], S, D)
        u[c] = 2 * rand(rng[c], S) - 1
    end

    partition = _shuffle_folds(Random.Xoshiro(rand(UInt32)), chains, chainsperfold)

    I = warmup + iterations
    allem = Array{S}(undef, I, chains, D)
    accepted = zeros(Bool, chains, I)
    # sigma = zeros(S, I, D, folds)
    # stepsize = zeros(S, I, folds)
    # damping = zeros(S, I, folds)
    # noise = zeros(S, I, folds)
    # drift = zeros(S, I, folds)
    acceptanceprob = zeros(S, chains, I)
    divergence = zeros(Bool, chains, I)
    return (sampler = MEADSSampler(),
            initialθ,
            f,
            AD,
            D,
            iterations,
            warmup,
            chains,
            nt,
            datas,
            rng,
            lp,
            grads,
            folds,
            chainsperfold,
            partition,
            m,
            σ,
            ε,
            εf,
            γ,
            α,
            δ,
            u,
            adam_steps,
            η,
            β1,
            β2,
            τ,
            adaptε,
            adaptσ,
            adaptγ,
            adaptα,
            adaptδ,
            accepted,
            acceptanceprob,
            # sigma,
            # stepsize,
            # damping,
            # noise,
            # drift,
            nru,
            allem,
            divergence,
            maxdeltaH,
            trajectorylengths,
            kwargs...)
end

function _kernel!(as::AbstractMEADSSampler, i, meads, θs)

    if i % meads.folds == 0
        srng = Random.Xoshiro(rand(UInt32))
        meads.partition .= _shuffle_folds(srng, meads.chains, meads.chainsperfold)
    end

    skipfold = i % meads.folds + 1
    nt = meads.nt
    @sync for it in 1:nt
        Threads.@spawn for f in it:nt:length(meads.partition)

            k = (f + 1) % meads.folds + 1
            kfold = meads.partition[k]

            Θ, σ = _standardize_draws(θs, kfold)
            meads.adaptσ && _update_metric!(meads.sampler, i, meads, f, σ)
            meads.adaptε && _update_stepsize!(meads.sampler, i, meads, f, θs, kfold, σ, it)
            meads.adaptγ && _update_damping!(i, meads, f, Θ)
            meads.adaptα && _update_noise!(i, meads, f)
            meads.adaptδ && _update_drift!(i, meads, f)

            fold = meads.partition[f]
            for c in fold
                if f != skipfold
                    # without @views indexed arrays are temporaries, not the originals. Shit.
                    @views info = pghmc!(i, θs[c][1], θs[c][2], meads.rng[c], meads.D,
                                         meads.m[:, c], meads.δ[f], meads.u[c:c], meads.α[f],
                                         meads.σ[:, f], meads.ε[f], meads.grads[:, c],
                                         meads.maxdeltaH, meads.nru, meads.lp[it],
                                         meads.datas[it])
                    updateinfo!(i, c, meads, info)
                else
                    θs[c][2] .= NaN # θs[c][1]
                end
            end
        end
    end
end

function pghmc!(i, θ1, θ2, rng, D, m, δ, u, α, σ, ε, g, maxdeltaH, nru, lp, data)
    T = eltype(θ1)
    onehalf = convert(T, 0.5)
    q = copy(θ1)
    mt = copy(m)

    val_grad!(lp, q)
    g .= grad(lp) # TODO(ear) does this need copy (.=), or can it just assign since the memory is already stored within lp?

    H1 = val(lp) + onehalf * (m' * m)
    isnan(H1) && (H1 = typemax(T))

    e = ε .* σ
    @. m -= onehalf * e * g
    @. q += e * m
    val_grad!(lp, q)
    g .= grad(lp)
    @. m -= onehalf * e * g

    H2 = val(lp) + onehalf * (m' * m)
    isnan(H2) && (H2 = typemax(T))
    divergent = divergence(H2, H1, maxdeltaH)

    a = H1 - H2
    accepted = log(abs(u[1])) < a

    allem = q
    @. θ2 = q * accepted + θ1 * (1 - accepted)
    @. m = m * accepted + -mt * (1 - accepted)
    m .= sqrt(1 - α ^ 2) .* m .+ α .* randn(rng, T, D)

    @. u = u * exp(-a) * accepted + u * (1 - accepted)
    v = abs(u[1])
    u[1] = nru ? (u[1] + 1 + δ) % 2 - 1 : rand(rng, T)

    return (accepted = accepted, divergence = divergent, v = v, allem = allem)
end

function _updateinfo!(as::AbstractMEADSSampler, i, c, meads, info)
    meads.accepted[c, i] = info.accepted
    meads.divergence[c, i] = info.divergence
    meads.acceptanceprob[c, i] = info.v
    meads.allem[i, c, :] .= info.allem
    # meads.sigma[i, :, :] .= meads.σ
    # meads.stepsize[i, :] .= meads.ε
    # meads.damping[i, :] .= meads.γ
    # meads.noise[i, :] .= meads.α
    # meads.drift[i, :] .= meads.δ
    # meads.momentum[i, :, :] .= meads.m
end

function _standardize_draws(θs, _fold)
    Θ = reduce(hcat, getindex.(θs[_fold], 1))'
    σ = std(Θ, dims = 1)
    Θ ./= reshape(σ, 1, :)
    μ = mean(Θ, dims = 1)
    Θ .-= μ
    σ = reshape(σ, :)
    return Θ, σ
end

function maxeigenvalue(x)
    N = size(x, 1)
    trace_est = sum(z -> z ^ 2, x)
    trace_sq_est = sum(z -> z ^ 2, x * x') / N
    return trace_sq_est / trace_est
end

function maxeigenvalue(x, xxp)
    N = size(x, 1)
    trace_est = sum(z -> z ^ 2, x)
    trace_sq_est = sum(z -> z ^ 2, xxp) / N # x * x'
    return trace_sq_est / trace_est
end

function _update_metric!(as::AbstractMEADSSampler, i, meads, f, s)
    meads.σ[:, f] .= s
end

function _update_stepsize!(as::AbstractMEADSSampler, i, meads, f, θs, _fold, σ, it)
    for c in _fold
        val_grad!(meads.lp[it], θs[c][1])
        meads.grads[:, c] .= grad(meads.lp[it])
    end
    g = meads.grads[:, _fold] .* σ
    # mul!(meads.xxp, g', g)
    meads.ε[f] = min(1, meads.εf / sqrt(maxeigenvalue(g')))
end

function _update_damping!(i, meads, f, Θ)
    # mul!(meads.xxp, Θ, Θ')
    meads.γ[f] = max(1 / i, meads.ε[f] / sqrt(maxeigenvalue(Θ)))
end

function _update_noise!(i, meads, f)
    meads.α[f] = sqrt(1 - exp(-2 * meads.γ[f]))
end

function _update_drift!(i, meads, f)
    meads.δ[f] = 0.5 * meads.α[f] ^ 2
end

function _shuffle_folds(rng, num_chains, num_chainsperfold)
    permchains = Random.randperm(rng, num_chains)
    partchains = Iterators.partition(permchains, num_chainsperfold)
    return collect(partchains)
end

function adam_initialize!(meads, θs)
    D = meads.D
    nt = meads.nt
    @sync for it in 1:nt
        Threads.@spawn for c in it:nt:meads.chains
            m = zeros(D)
            v = zeros(D)
            for i in 1:meads.adam_steps
                val_grad!(meads.lp[it], θs[c][1])
                meads.grads[:, c] .= grad(meads.lp[it])
                θs[c][1] .-= _adam_step!(i, meads.grads[:, c], m, v, meads.η, meads.β1, meads.β2, meads.τ)
            end
        end
    end
end
