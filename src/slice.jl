abstract type AbstractSliceSampler <: AbstractSampler end

struct SliceSampler <: AbstractSliceSampler end
struct SliceDSampler <: AbstractSliceSampler end

function Slice(initialθ,
               parameters;
               iterations = 2000,
               idx = getindices(initialθ, parameters),
               pdim = length(idx),
               m = typemax(Int),
               w = 1.0,
               lower = repeat([typemin(eltype(initialθ))], pdim),
               upper = repeat([typemax(eltype(initialθ))], pdim))
    typeof(lower) <: Number && (lower = [lower])
    typeof(upper) <: Number && (upper = [upper])

    length(lower) == 1 && (lower = repeat(lower, pdim))
    length(upper) == 1 && (upper = repeat(upper, pdim))

    length(lower) != pdim && error("Length of lower limit must match the length of paramters.")
    length(upper) != pdim && error("Length of upper limit must match the length of paramters.")

    L = similar(initialθ)
    R = similar(initialθ)
    return (sampler = SliceSampler(),
            parameters = parameters,
            iterations = iterations,
            idx = idx,
            pdim = pdim,
            m = m,
            w = w,
            lower = lower,
            upper = upper,
            L = similar(initialθ),
            R = similar(initialθ),
            evaluated = zeros(Int, iterations))
end

function SliceD(initialθ,
                parameters;
                iterations = 2000,
                idx = getindices(initialθ, parameters),
                pdim = length(idx),
                m = typemax(Int),
                w = 1.0,
                lower = repeat([typemin(eltype(initialθ))], pdim),
                upper = repeat([typemax(eltype(initialθ))], pdim))
    slice = Slice(initialθ,
                  parameters;
                  iterations = iterations,
                  idx = idx,
                  m = m,
                  w = w,
                  lower = lower,
                  upper = upper)
    return merge(slice, (sampler = SliceDSampler(),))
end

function _kernel!(as::AbstractSliceSampler, i, slice, θs, lp;
                  data = nothing, kwargs...)
    old = θs[i-1]
    new = θs[i]

    idx = slicer.idx
    lower = slicer.lower
    upper = slicer.upper
    L = slicer.L
    R = slicer.R
    w = slicer.w
    m = slicer.m

    L .= old
    R .= old

    evals = 0

    for (j, k) in pairs(slicer.idx)
        # adapted from Radford M. Neal
        # https://www.cs.toronto.edu/~radford/ftp/slice-R-prog
        # retrieved on 2021-07-17

        # TODO insert checks

        # need external draws[i] .= draws[i-1] so that
        # if i == first(slicer.idx) => θs[2] == θs[1]
        # else θs[2] is the most recent draws
        logy = lp(new, data) + log(rand())
        evals += 1

        u = initialinterval(slicer)
        L[k] = new[k] - u
        R[k] = new[k] + (w - u)

        # expand interval
        if slicer.m == typemax(Int)
            while true
                L[k] <= lower[j] && break
                lp(L, data) <= logy && break
                evals += 1
                L[k] -= w
            end

            while true
                R[k] >= upper[j] && break
                lp(R, data) <= logy && break
                evals += 1
                R[k] += w
            end
        elseif m > 1
            J = floor(rand() * m)
            K = (m - 1) - J

            while J > 0
                L[k] <= lower[j] && break
                lp(L, data) <= logy && break
                evals += 1
                L[k] -= w
                J -= 1
            end

            while K > 0
                R[k] >= upper[j] && break
                lp(R, data) <= logy && break
                evals += 1
                R[k] += w
                K -= 1
            end
        end

        # if known bounds,
        # shrink interval to lower and upper bounds
        L[k] < lower[j] && (L[k] = lower[j])
        R[k] > upper[j] && (R[k] = upper[j])

        # sample from the interval, shrinking upon rejection
        while true
            new[k] = sampleinterval(slicer, k)
            pθ2 = lp(new, data)
            evals += 1
            pθ2 >= logy && break
            if new[k] > old[k]
                R[k] = new[k]
            else
                L[k] = new[k]
            end
        end

        # update bounds to most recent point
        # ensures interval expansion on the slice most recently sampled
        L[k] = new[k]
        R[k] = new[k]
    end
    return (evaluated = evals,)
end

function initialinterval(slicer::SliceSampler)
    return rand() * slicer.w
end

function initialinterval(slicer::SliceDSampler)
    return rand(0:slicer.w)
end

function sampleinterval(slicer::SliceSampler, i)
    return slicer.L[i] + rand() * (slicer.R[i] - slicer.L[i])
end

function sampleinterval(slicer::SliceDSampler, i)
    return  rand(slicer.L[i]:slicer.R[i])
end


function _updateinfo!(as::AbstractSliceSampler, i, slice, info)
    slice.evaluated[i] = info.evaluated
end
