abstract type AbstractChEESRSampler <: AbstractSGASampler end
struct ChEESRHMCSampler <: AbstractChEESSampler end

function SGA(::Val{:cheesr}; kwargs...)
    return (sampler = ChEESRHMCSampler(), kwargs...)
end

function _sampler_gradient!(as::AbstractChEESRSampler, i, sga, θs, mθ, mq)
    t = sga.knobs.T + sga.knobs.ε # [4]#L643
    h = halton(i)
    q = Vector{eltype(θs[1][1])}(undef, sga.D)
    @inbounds @simd for c in 1:sga.chains
        q .= sga.q[:, c]
        dsq = _csum(abs2, q, mq) - _csum(abs2, θs[c][1], mθ)
        sga.ghats[c] = 4 * dsq * _cdot(q, mq, sga.p[:, c]) - dsq ^ 2 / t
        sga.ghats[c] *= h
    end
end
